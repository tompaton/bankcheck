#!/usr/bin/env python
# coding: utf-8

from pathlib import Path
from bankcheck import analysis, database, process


def import_csvs(db):
    for glob, account_name, account_number in [
            ('StatementCsv1-*.csv', 'First', '12345678'),
            ('StatementCsv2-*.csv', 'Second', '23456789')]:
        account_id = database.insert_account(db, account_name, account_number)
        statement_csvs = Path("bank-csvs/").glob(glob)
        for statement in statement_csvs:
            print('{} {}: {}'.format(account_id, account_name, statement))
            process.import_csv_file(db, account_id, str(statement))


def insert_labels(db):
    db.execute('DELETE FROM labels')
    db.execute('DELETE FROM label_rules')

    process.insert_default_labels(db)

    for regex, *label_names in [
            (r'safeway', 'supermarket'),
            (r'bp', 'petrol', 'car'),
    ]:
        for label_name in label_names:
            if type(label_name) is tuple:
                label_name, label_display = label_name
            else:
                label_display = None
            database.insert_label(db, label_name, regex, label_display)

    for card_number, *label_names in [
            ('1234567', ('card-one', '👩')),
            ('2345678', ('card-two', '👨')),
    ]:
        for label_name in label_names:
            if type(label_name) is tuple:
                label_name, label_display = label_name
            else:
                label_display = None
            process.insert_card_label(db, label_name, card_number, label_display)


def insert_cleaning_rules(db):
    db.execute('DELETE FROM cleaning_rules')

    process.insert_default_cleaning_rules(db)

    for regex, subst in [
        (r'12345678', '<acc-1st>'),
        (r'23456789', '<acc-2nd>'),
    ]:
        database.insert_cleaning_rule(db, regex, subst)


if __name__ == '__main__':
    db = database.init_db('bank.db')
    with db:
        import_csvs(db)

        # process.reprocess_descriptions(db)
        insert_cleaning_rules(db)
        process.cleanup_descriptions(db)

        # process.reprocess_labels(db)
        insert_labels(db)
        process.label_transactions(db)

        analysis.cleaning_rules_used(db)
        analysis.label_rules_used(db)
        analysis.labels_used(db)
        analysis.unlabelled_transactions(db)

        analysis.transactions_view(db, N=30)

        analysis.one_off_credits(db, N=30)
        analysis.one_off_debits(db, N=30)
        analysis.recurring_credits(db, N=30)
        analysis.recurring_debits(db, N=30)

        analysis.last_entered_date(db)
