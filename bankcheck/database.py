import sqlite3
from datetime import date, timedelta


def init_db(db_filename):
    db = sqlite3.connect(db_filename)
    db.row_factory = sqlite3.Row

    db.execute('''CREATE TABLE IF NOT EXISTS
                  migration_log (
                    migration_name TEXT UNIQUE
                  )''')

    db.execute('''CREATE TABLE IF NOT EXISTS
                  accounts (
                    account_name TEXT UNIQUE,
                    account_number TEXT
                  )''')

    db.execute('''CREATE TABLE IF NOT EXISTS
                  labels (
                    label_name TEXT UNIQUE,
                    label_display TEXT
                  )''')

    db.execute('''CREATE TABLE IF NOT EXISTS
                  cleaning_rules (
                    regex TEXT UNIQUE,
                    subst TEXT
                  )''')

    db.execute('''CREATE TABLE IF NOT EXISTS
                  label_rules (
                    label_id INTEGER REFERENCES labels (rowid),
                    regex TEXT,
                    UNIQUE (label_id, regex)
                  )''')

    db.execute('''CREATE TABLE IF NOT EXISTS
                  transactions (
                    account_id INTEGER REFERENCES accounts (rowid),
                    entered_date TEXT,
                    iso_entered_date TEXT,
                    description TEXT,
                    amount NUMERIC,
                    balance NUMERIC,
                    amount_str TEXT,
                    balance_str TEXT,
                    date_imported INTEGER,
                    UNIQUE (account_id, entered_date, description, amount)
                  )''')

    db.execute('''CREATE TABLE IF NOT EXISTS
                  cleaned_descriptions (
                    transaction_id INTEGER REFERENCES transactions (rowid),
                    cleaned_desc TEXT,
                    UNIQUE (transaction_id)
                  )''')

    db.execute('''CREATE TABLE IF NOT EXISTS
                  cleaning_rules_used (
                    transaction_id INTEGER REFERENCES transactions (rowid),
                    cleaning_rule_id INTEGER REFERENCES cleaning_rules (rowid),
                    UNIQUE (transaction_id, cleaning_rule_id)
                  )''')

    db.execute('''CREATE TABLE IF NOT EXISTS
                  labelled_transactions (
                    transaction_id INTEGER REFERENCES transactions (rowid),
                    label_id INTEGER REFERENCES labels (rowid),
                    UNIQUE (transaction_id, label_id)
                  )''')

    db.execute('''CREATE TABLE IF NOT EXISTS
                  label_rules_used (
                    transaction_id INTEGER REFERENCES transactions (rowid),
                    label_rule_id INTEGER REFERENCES label_rules (rowid),
                    UNIQUE (transaction_id, label_rule_id)
                  )''')

    create_transactions_view(db)

    # use NOT IN to get recurring transactions
    # not enough truly repeating transactions to be worth splitting from
    # varying
    db.execute('''CREATE VIEW IF NOT EXISTS
                  one_off_transactions
                  AS SELECT transaction_id
                       FROM cleaned_descriptions
                   GROUP BY cleaned_desc
                     HAVING COUNT(*) = 1''')

    # NOTE: amounts is rounded and absolute values
    db.execute('''CREATE VIEW IF NOT EXISTS
                  all_amounts
                  AS SELECT D.cleaned_desc,
                            GROUP_CONCAT(REPLACE(ROUND(ABS(T.amount),0),
                                                 '.0','')) AS amounts
                       FROM cleaned_descriptions D
                            INNER JOIN transactions T
                              ON T.rowid = D.transaction_id
                   GROUP BY D.cleaned_desc''')

    do_migrations(db)

    return db


def create_transactions_view(db):
    db.execute('''CREATE VIEW IF NOT EXISTS
                  transactions_view
                  AS SELECT T.account_id, T.rowid AS transaction_id,
                            T.date_imported,
                            T.entered_date, T.iso_entered_date, T.amount,
                            T.description, T.amount_str,
                            D.cleaned_desc,
                            COALESCE(GROUP_CONCAT(COALESCE(L.label_display,
                                                           L.label_name)), '')
                            AS label_names
                       FROM transactions T
                            INNER JOIN cleaned_descriptions D
                              ON T.rowid = D.transaction_id
                            LEFT OUTER JOIN labelled_transactions LT
                              ON T.rowid = LT.transaction_id
                            LEFT OUTER JOIN labels L
                              ON LT.label_id = L.rowid
                   GROUP BY T.rowid
                   ORDER BY iso_entered_date''')


def do_migrations(db):
    migration_log \
        = {row[0] for row in db.execute('SELECT * FROM migration_log')}

    for migration in [migration_label_display,
                      migration_transactions_view2,
                      migration_transactions_view3]:
        if migration.__name__ not in migration_log:
            migration(db)
            db.execute('INSERT INTO migration_log (migration_name) VALUES (?)',
                       (migration.__name__,))


def migration_label_display(db):
    db.execute('ALTER TABLE labels ADD label_display TEXT')


def migration_transactions_view2(db):
    db.execute('DROP VIEW transactions_view')
    create_transactions_view(db)


def migration_transactions_view3(db):
    db.execute('DROP VIEW transactions_view')
    create_transactions_view(db)


def insert_account(db, account_name, account_number):
    return _insert_row(db, 'accounts',
                       account_name=account_name,
                       account_number=account_number)


def insert_transaction(db, account_id, date_imported,
                       entered_date, iso_entered_date, description, amount,
                       balance):
    amount_str = format_curr(amount)
    balance_str = format_curr(balance) if balance else None
    return _insert_row(db, 'transactions',
                       account_id=account_id,
                       date_imported=date_imported,
                       entered_date=entered_date,
                       iso_entered_date=iso_entered_date,
                       description=description,
                       amount=amount,
                       amount_str=amount_str,
                       balance=balance,
                       balance_str=balance_str,
                       insert_only_cols=('date_imported',
                                         'amount_str',
                                         'balance', 'balance_str'))


def format_curr(amount):
    return '{}${:,.2f}'.format('-' * amount.startswith('-'),
                               abs(float(amount)))


def insert_label(db, label_name, regex, label_display=None):
    label_id = _insert_row(db, 'labels', label_name=label_name,
                           label_display=label_display,
                           insert_only_cols=('label_display',))

    if regex:
        _insert_row(db, 'label_rules', label_id=label_id, regex=regex)

    return label_id


def insert_cleaning_rule(db, regex, subst):
    _insert_row(db, 'cleaning_rules', regex=regex, subst=subst)


def insert_cleaning_rules_used(db, transaction_id, cleaning_rule_id):
    _insert_row(db, 'cleaning_rules_used',
                transaction_id=transaction_id,
                cleaning_rule_id=cleaning_rule_id)


def insert_cleaned_description(db, transaction_id, cleaned_desc):
    _insert_row(db, 'cleaned_descriptions',
                transaction_id=transaction_id,
                cleaned_desc=cleaned_desc)


def label_transaction(db, transaction_id, label_id, label_rule_id):
    _insert_row(db, 'labelled_transactions',
                transaction_id=transaction_id,
                label_id=label_id)

    if label_rule_id:
        _insert_row(db, 'label_rules_used',
                    transaction_id=transaction_id,
                    label_rule_id=label_rule_id)


def _insert_row(db, table, insert_only_cols=(), **values):
    try:
        db.execute('INSERT INTO {} ({}) VALUES ({})'
                   .format(table,
                           ','.join(values.keys()),
                           ','.join('?' for col in values.keys())),
                   tuple(values.values()))
    except:
        cols = [col for col in values.keys() if col not in insert_only_cols]
        return list(db.execute(
            'SELECT rowid FROM {} WHERE {}'
            .format(table, ' AND '.join('{}=?'.format(col) for col in cols)),
            tuple(values[col] for col in cols)))[0][0]
    else:
        return list(db.execute('SELECT last_insert_rowid()'))[0][0]


def load_cleaning_rules(db):
    return list(db.execute('SELECT rowid AS rule_id, regex, subst '
                           'FROM cleaning_rules'))


def load_label_rules(db):
    return list(db.execute('SELECT rowid AS rule_id, regex, label_id '
                           'FROM label_rules'))


def load_unprocessed_transactions(db):
    return list(db.execute('SELECT rowid AS transaction_id, description '
                           'FROM transactions '
                           'WHERE rowid NOT IN ('
                           'SELECT transaction_id FROM cleaned_descriptions'
                           ')'))


def load_unlabelled_transactions(db, filter_debit_credit=False):
    if filter_debit_credit:
        where = 'WHERE label_id NOT IN (?, ?)'
        params = (insert_label(db, 'credit', None),
                  insert_label(db, 'debit', None))
    else:
        where = ''
        params = ()

    return list(db.execute('SELECT T.* '
                           'FROM transactions_view T '
                           'WHERE T.transaction_id NOT IN ('
                           'SELECT transaction_id FROM labelled_transactions '
                           + where + ')',
                           params))


def delete_cleaned_descriptions(db):
    db.execute('DELETE FROM cleaned_descriptions')
    db.execute('DELETE FROM cleaning_rules_used')


def delete_labelled_transactions(db):
    db.execute('DELETE FROM labelled_transactions')
    db.execute('DELETE FROM label_rules_used')


def cleaning_rules_used(db):
    return list(db.execute('SELECT R.rowid AS rule_id, R.regex, R.subst, '
                           'COUNT(U.rowid) AS count '
                           'FROM cleaning_rules R '
                           'LEFT OUTER JOIN cleaning_rules_used U '
                           'ON R.rowid = U.cleaning_rule_id '
                           'GROUP BY R.rowid'))


def label_rules_used(db):
    return list(db.execute('SELECT R.rowid AS rule_id, R.regex, R.label_id, '
                           'L.label_name, '
                           'COUNT(U.rowid) AS count '
                           'FROM label_rules R '
                           'INNER JOIN labels L '
                           'ON R.label_id = L.rowid '
                           'LEFT OUTER JOIN label_rules_used U '
                           'ON R.rowid = U.label_rule_id '
                           'GROUP BY R.rowid'))


def labels_used(db):
    return list(db.execute('SELECT L.rowid AS label_id, L.label_name, '
                           'COUNT(LT.transaction_id) AS count '
                           'FROM labels L '
                           'LEFT OUTER JOIN labelled_transactions LT '
                           'ON L.rowid = LT.label_id '
                           'GROUP BY L.rowid'))


def transactions_view(db):
    return list(db.execute('SELECT * FROM transactions_view'))


def one_off_transactions(db, label_id):
    return list(db.execute('SELECT * FROM transactions_view T '
                           'INNER JOIN one_off_transactions O '
                           'ON T.transaction_id = O.transaction_id '
                           'INNER JOIN labelled_transactions L '
                           'ON T.transaction_id = L.transaction_id '
                           'WHERE L.label_id = ?',
                           (label_id,)))


def recurring_transactions(db, label_id):
    return list(db.execute('SELECT T.*, A.amounts '
                           'FROM transactions_view T '
                           'INNER JOIN labelled_transactions L '
                           'ON T.transaction_id = L.transaction_id '
                           'INNER JOIN all_amounts A '
                           'ON T.cleaned_desc = A.cleaned_desc '
                           'WHERE L.label_id = ? '
                           'AND T.transaction_id NOT IN ('
                           'SELECT transaction_id FROM one_off_transactions'
                           ')',
                           (label_id,)))


def latest_transactions(db, account_id, date_imported=None, days=None,
                        _today=None):
    sql = ('SELECT T.*, A.amounts '
           'FROM transactions_view T '
           'INNER JOIN all_amounts A '
           'ON T.cleaned_desc = A.cleaned_desc '
           'WHERE T.account_id = ?')

    if date_imported:
        sql += ' AND T.date_imported >= ?'
        params = (account_id, date_imported.date())

    elif days:
        date_to = _today or date.today()
        date_from = date_to - timedelta(days=days)

        sql += ' AND T.iso_entered_date >= ?'
        params = (account_id, '{0:%Y-%m-%d}'.format(date_from))

    else:
        params = (account_id,)

    return list(db.execute(sql, params))


def get_accounts(db):
    return list(db.execute('SELECT A.rowid AS account_id, '
                           'A.account_name, A.account_number, '
                           '(SELECT iso_entered_date FROM transactions '
                           'WHERE account_id=A.rowid '
                           'ORDER BY iso_entered_date DESC, '
                           'rowid DESC LIMIT 1) '
                           'AS last_iso_entered_date '
                           'FROM accounts A'))


def get_account_balance(db, account_id):
    return list(db.execute('SELECT balance_str FROM transactions '
                           'WHERE account_id=? '
                           'ORDER BY iso_entered_date DESC, '
                           'rowid DESC LIMIT 1',
                           (account_id,)))
