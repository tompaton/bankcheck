# coding: utf-8

import re
from datetime import date
from unittest.mock import sentinel, patch, call, Mock, MagicMock

import pytest

from . import process


@pytest.fixture
def reverse_rules():
    return False


@pytest.fixture
def cleaning_rules(reverse_rules):
    # run each test with normal and reversed order of rules
    rules = enumerate(process.DEFAULT_CLEANING_RULES)
    if reverse_rules:
        rules = reversed(list(rules))
    return [{'rule_id': i + 1, 'regex': re.compile(regex), 'subst': subst}
            for i, (regex, subst) in rules]


@pytest.fixture
def label_rules():
    label_ids = []
    for label, regex, *label_display in process.DEFAULT_LABELS:
        if label not in label_ids:
            label_ids.append(label)

    return [{'rule_id': i + 101,
             'regex': re.compile(regex, re.IGNORECASE),
             'label_display': label_display[0] if label_display else None,
             'label_id': label_ids.index(label) + 1}
            for i, (label, regex, *label_diplay)
            in enumerate(process.DEFAULT_LABELS)
            if regex]


@pytest.mark.parametrize('reverse_rules', [(False,), (True,)])
@pytest.mark.parametrize('dirty,cleaned,used,used_reverse', [
    ('', '', [], []),

    # rule 1
    ("Osko Payment To Lorum Account 123456 ABC - Ipsum Dolor Ref#0123456789",
     "osko payment to lorum account 123456 abc - ipsum dolor <ref>",
     [1], [1]),

    # rule 1 and 11 overlap
    # test <ref> and <card> rules in both orders
    ("VISA-LORUM IPSUM DOLOR    SIT AMET     AU#2345678(Ref.0123456789)",
     "visa-lorum ipsum dolor sit amet ",
     [1, 11], [11]),
    ("VISA-PAYPAL *LORUM             1234567   US#2345678(Ref.0123456789)",
     "visa-paypal *lorum 1234567 ",
     [1, 11], [11]),

    # rule 2
    ("Member Net transfer - Rec. No.: 0123456789. "
     "To Lorum Ipsum Dolor , 12345678 - B",
     "member net transfer <ref>to lorum ipsum dolor , 12345678 - b",
     [2], [2]),
    ("Member Net transfer received from SAV 12345678. Receipt No.: 0123456789 ",
     "member net transfer received from sav 12345678. <ref>",
     [2], [2]),
    ("Member Net transfer to 12345678. Receipt No.: 0123456789 ",
     "member net transfer to 12345678. <ref>",
     [2], [2]),

    # rule 2, 8 and 10 overlap
    # test rules in both orders
    ("Internet BPay to lorum ipsum dolor - Biller Code 98765"
     " - Receipt No 0123456789 CRN#0123456",
     "bpay-lorum ipsum dolor - biller 98765 <ref>",
     [2, 8, 10], [2, 8, 10]),
    ("Internet BPay to LORUM IPSUM DOLOR - Biller Code 98765"
     " - Receipt No 0123456789",
     "bpay-lorum ipsum dolor - biller 98765 <ref>",
     [2, 8, 10], [2, 8, 10]),

    # rule 3
    ("Ext TFR - NET# 0123456789 to 1234567 Lorum Ipsum Dolor ABC - Sit Amet",
     "ext tfr <ref> lorum ipsum dolor abc - sit amet",
     [3], [3]),

    # rule 4
    ("ATM#123456-LORUM - 330 IPSUM STREET",
     "atm-lorum - 330 ipsum street",
     [4], [4]),

    # rule 5
    (r"ATM Owner Fee 123456 LORUM\IPSUM                         AU",
     r"atm fee-lorum\ipsum au",
     [5], [5]),

    # rule 6
    ("POS #123456-LORUM IPSUM DOLOR     12345 SIT AMET VIC",
     "pos-lorum ipsum dolor 12345 sit amet vic",
     [6], [6]),

    # rule 7
    (r"POS Including Cash Out#123456-LORUM      3205\IPSUM VIC      AU",
     r"pos+cash-lorum 3205\ipsum vic au",
     [7], [7]),

    # rule 8 - tested with rule 2

    # rule 9
    ("Bpay payment to LORUM IPSUM DOLOR 123456",
     "bpay-lorum ipsum dolor - biller 123456",
     [9], [9]),

    # rule 10 - tested with rule 2

    # rule 11 - tested with rule 1
])
def test_cleanup_description(cleaning_rules, dirty, cleaned,
                             used, used_reverse, reverse_rules):
    result, rules_used = process.cleanup_description(dirty, cleaning_rules)
    assert result == cleaned
    if reverse_rules:
        assert sorted(rules_used) == used_reverse
    else:
        assert sorted(rules_used) == used


@patch.object(process, 'database')
def test_cleanup_descriptions(mock_database):
    mock_database.load_cleaning_rules.return_value \
        = [{'rule_id': 1, 'regex': 'abc', 'subst': '1'},
           {'rule_id': 2, 'regex': 'def', 'subst': '2'}]

    mock_database.load_unprocessed_transactions.return_value \
        = [{'transaction_id': 1, 'description': 'abc'},
           {'transaction_id': 2, 'description': 'abc def'}]

    process.cleanup_descriptions(sentinel.db)

    assert mock_database.mock_calls \
        == [call.load_cleaning_rules(sentinel.db),
            call.load_unprocessed_transactions(sentinel.db),
            call.insert_cleaned_description(sentinel.db, 1, '1'),
            call.insert_cleaning_rules_used(sentinel.db, 1, 1),
            call.insert_cleaned_description(sentinel.db, 2, '1 2'),
            call.insert_cleaning_rules_used(sentinel.db, 2, 1),
            call.insert_cleaning_rules_used(sentinel.db, 2, 2)]


@patch.object(process, 'database')
def test_insert_card_label(mock_database):
    process.insert_card_label(sentinel.db, 'card-label', '123456')

    assert mock_database.mock_calls == [
        call.insert_label(
            sentinel.db, 'card-label',
            r'(au|gb|us|lu|nl|hk)(|frgn amt-[\d\.]+)#123456', None),
        call.insert_label(
            sentinel.db, 'card-label',
            r'visa access card fee <va\s*-12345\s*>', None)]


@patch.object(process, 'database')
def test_insert_card_label_display(mock_database):
    process.insert_card_label(sentinel.db, 'card-label', '123456', '💳')

    assert mock_database.mock_calls == [
        call.insert_label(
            sentinel.db, 'card-label',
            r'(au|gb|us|lu|nl|hk)(|frgn amt-[\d\.]+)#123456', '💳'),
        call.insert_label(
            sentinel.db, 'card-label',
            r'visa access card fee <va\s*-12345\s*>', '💳')]


def test_card_label_regex():
    assert list(process.card_label_regex('123456')) == [
        r'(au|gb|us|lu|nl|hk)(|frgn amt-[\d\.]+)#123456',
        r'visa access card fee <va\s*-12345\s*>',
    ]


@pytest.mark.parametrize('description', [
    'VISA-LORUM IPSUM    DOLOR     AU#1234567(Ref.0123456789)',
    'VISA-LORUM *IPSUM         3456789123  GB#1234567(Ref.0123456789)',
    'VISA-LORUM *IPSUM           3456789123   US#1234567(Ref.0123456789)',
    'VISA-LORUM IPSUM    3456789123   USFRGN AMT-50.000000#1234567(Ref.0123456789)',
    'VISA-LORUM IPSUM     DOLOR.SIT   LU#1234567(Ref.0123456789)',
    'VISA-LORUM.IPSUM.COM          3456789123  NL#1234567(Ref.0123456789)',
    'VISA-PAYPAL *LORUM IPSUM        3456789123   HK#1234567(Ref.0123456789)',
])
def test_card_label_regex_match_visa(description):
    regex = re.compile(list(process.card_label_regex('1234567'))[0],
                       re.IGNORECASE)
    assert regex.search(description)


@pytest.mark.parametrize('description', [
    'Visa Access Card Fee <VA -123456      > ',
])
def test_card_label_regex_match_visa_fee(description):
    regex = re.compile(list(process.card_label_regex('1234567'))[1],
                       re.IGNORECASE)
    assert regex.search(description)


@pytest.mark.parametrize('description,amount,result', [

    # unlabelled

    ('', 0, []),
    ('lorum ipsum dolor sit amet', 0, []),

    # debit/credit

    ('lorum ipsum dolor sit amet', 100, [(1, None)]),
    ('lorum ipsum dolor sit amet', -100, [(2, None)]),

    # zero amount so debit/credit isn't labelled

    ('Member Net transfer - Rec. No.: 01234567. '
     'To Lorum Ipsum , 1234567 - ABC -',
     0, [(3, 103)]),
    ("Invalid PIN #55 - LORUM IPSUM", 0, [(4, 105)]),
    ("Rate Change: 12.3400% To 23.4500%", 0, [(4, 106)]),
    ("Transaction Fee ", 0, [(5, 107)]),
    ("Visa Access Card Fee <VA -123456      > ", 0, [(5, 108), (7, 115)]),
    ("Overdraft Administration Fee ", 0, [(5, 109)]),
    ("Overdraft Interest ", 0, [(5, 110)]),
    ("Bank Cheque Fee", 0, [(5, 111)]),
    (r"ATM#123456-766 LORUM ROAD       \IPSUM DOLOR  AU", 0, [(6, 112)]),
    (r"ATM Owner Fee 12345 LORUM\IPSUM                         AU",
     0, [(6, 113), (5, 114)]),
    ("VISA-LORUM PTY LTD        IPSUM     AU#1234567(Ref.0123456789)",
     0, [(7, 115)]),
    ("Direct Debit LORUM IPSUM DOLOR - 123456789", 0, [(8, 116)]),
    ("POS #12345-LORUM IPSUM     1234 DOLOR VIC", 0, [(9, 117)]),
    ("POS Including Cash Out#12345-LORUM IPSUM ABC03",
     0, [(9, 118), (10, 119)]),
    ("Internet BPay to lorum ipsum - Biller Code 12345"
     " - Receipt No 123456789 CRN#012345", 0, [(11, 120)]),
    ("Bpay payment to LORUM IPSUM DOLOR 12345", 0, [(11, 120)]),

    # check debit/credit also included

    ("Osko Payment From Lorum Ipsum", 100, [(1, None), (3, 104)]),
    ("Osko Payment To Lorum Ipsum Account 123456789 ABC Ref#1234567",
     -100, [(2, None), (3, 104)]),
])
def test_label_transaction(label_rules, description, amount, result):
    assert list(process.label_transaction(label_rules, 1, 2,
                                          description, amount)) \
        == result


@patch.object(process, 'database')
def test_label_transactions(mock_database):
    mock_database.load_label_rules.return_value \
        = [{'rule_id': 1, 'regex': 'abc', 'label_id': 11},
           {'rule_id': 2, 'regex': 'def', 'label_id': 12}]

    mock_database.insert_label.side_effect = [21, 22]  # credit, debit

    mock_database.load_unlabelled_transactions.return_value \
        = [{'transaction_id': 1, 'description': 'abc', 'amount': 100},
           {'transaction_id': 2, 'description': 'abc def', 'amount': -100}]

    process.label_transactions(sentinel.db)

    assert mock_database.mock_calls \
        == [call.load_label_rules(sentinel.db),
            call.insert_label(sentinel.db, 'credit', None),
            call.insert_label(sentinel.db, 'debit', None),
            call.load_unlabelled_transactions(sentinel.db),
            call.label_transaction(sentinel.db, 1, 21, None),
            call.label_transaction(sentinel.db, 1, 11, 1),
            call.label_transaction(sentinel.db, 2, 22, None),
            call.label_transaction(sentinel.db, 2, 11, 1),
            call.label_transaction(sentinel.db, 2, 12, 2)]


@patch.object(process, 'import_csv')
@patch.object(process, 'cleanup_descriptions')
@patch.object(process, 'label_transactions')
def test_update_transactions(label_transactions,
                             cleanup_descriptions,
                             import_csv):
    db = sentinel.db
    bank = Mock()
    bank.get_csv.return_value = b'csv line1\nline 2'

    import_csv.return_value = '2018-06-03T01:02:03.456'

    assert process.update_transactions(db, bank, 1, '1234', '2018-06-01',
                                       _today=date(2018, 6, 3)) \
        == '2018-06-03T01:02:03.456'

    bank.get_csv.assert_called_with('1234', date(2018, 6, 1), date(2018, 6, 3))

    import_csv.assert_called_with(sentinel.db, 1, ['csv line1', 'line 2'])

    cleanup_descriptions.assert_called_with(sentinel.db)

    label_transactions.assert_called_with(sentinel.db)


@patch.object(process, 'update_transactions')
@patch.object(process, 'database')
def test_latest_transactions(database, update_transactions):
    db = MagicMock()
    bank = Mock()

    account1 = {'account_id': 1,
                'account_number': '1234',
                'last_iso_entered_date': '2018-06-01'}
    account1b = dict(account1, last_balance_str='$12.34')

    account2 = {'account_id': 2,
                'account_number': '2345',
                'last_iso_entered_date': '2018-06-02'}
    account2b = dict(account2, last_balance_str='$23.45')

    database.get_accounts.return_value = [account1, account2]

    database.get_account_balance.side_effect \
        = [[{'balance_str': '$12.34'}],
           [{'balance_str': '$23.45'}]]

    database.latest_transactions.side_effect = [['one', 'two'],
                                                ['three', 'four']]

    update_transactions.side_effect = ['2018-06-03T01:02:03.456',
                                       '2018-06-04T02:03:04.567']

    assert list(process.latest_transactions(db, bank)) == [
        (account1b, ['one', 'two']),
        (account2b, ['three', 'four']),
    ]

    assert database.get_accounts.mock_calls == [call(db)]

    assert update_transactions.mock_calls \
        == [call(db, bank, 1, '1234', '2018-06-01'),
            call(db, bank, 2, '2345', '2018-06-02')]

    assert database.latest_transactions.mock_calls \
        == [call(db, 1, date_imported='2018-06-03T01:02:03.456'),
            call(db, 2, date_imported='2018-06-04T02:03:04.567')]

    assert database.get_account_balance.mock_calls \
        == [call(db, 1), call(db, 2)]


@patch.object(process, 'update_transactions')
@patch.object(process, 'database')
def test_latest_transactions_days(database, update_transactions):
    db = MagicMock()
    bank = Mock()

    account1 = {'account_id': 1,
                'account_number': '1234',
                'last_iso_entered_date': '2018-06-01'}
    account1b = dict(account1, last_balance_str='$12.34')

    database.get_accounts.return_value = [account1]

    database.get_account_balance.return_value \
        = [{'balance_str': '$12.34'}]

    database.latest_transactions.return_value = ['one', 'two']

    update_transactions.return_value = '2018-06-03T01:02:03.456'

    assert list(process.latest_transactions(db, bank, 5)) == [
        (account1b, ['one', 'two']),
    ]

    assert database.get_accounts.mock_calls == [call(db)]

    assert update_transactions.mock_calls \
        == [call(db, bank, 1, '1234', '2018-06-01')]

    assert database.latest_transactions.mock_calls \
        == [call(db, 1, days=5)]

    assert database.get_account_balance.mock_calls \
        == [call(db, 1)]
