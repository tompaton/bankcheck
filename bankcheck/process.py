# coding: utf-8

import csv
import re
from datetime import datetime, date
from functools import partial
from . import database


def latest_transactions(db, bank, days=None):
    with db:
        for account in database.get_accounts(db):
            date_imported \
                = update_transactions(db, bank, account['account_id'],
                                      account['account_number'],
                                      account['last_iso_entered_date'])
            if days:
                transactions \
                    = database.latest_transactions(
                        db, account['account_id'], days=days)
            else:
                transactions \
                    = database.latest_transactions(
                        db, account['account_id'], date_imported=date_imported)

            balance = database.get_account_balance(db, account['account_id'])

            yield (dict(account, last_balance_str=balance[0]['balance_str']),
                   transactions)


def update_transactions(db, bank, account_id, account_number,
                        last_iso_entered_date, _today=None):
    date_from = date(*map(int, last_iso_entered_date.split('-')))
    csv_data = bank.get_csv(account_number, date_from, _today or date.today())
    try:
        date_imported = import_csv(db, account_id,
                                   csv_data.decode('utf-8', 'replace').split('\n'))
    except:
        print('csv_data: %r', csv_data)
        raise
    cleanup_descriptions(db)
    label_transactions(db)
    return date_imported


def import_csv_file(db, account_id, csv_filename, **kwargs):
    with open(csv_filename, newline='') as csv_file:
        return import_csv(db, account_id, csv_file,
                          **kwargs)


def import_csv(db, account_id, csv_file,
               entered_date='Entered Date',
               description='Transaction Description',
               amount='Amount', balance='Balance'):
    date_imported = datetime.now()
    reader = csv.DictReader(csv_file, delimiter=',')
    for row in reader:
        iso_entered_date \
            = '{2:04d}-{1:02d}-{0:02d}'.format(
                *map(int, row[entered_date].split('/')))
        row_balance = row[balance] if balance in row else None
        database.insert_transaction(db, account_id, date_imported,
                                    row[entered_date], iso_entered_date,
                                    row[description], row[amount], row_balance)
    return date_imported


DEFAULT_LABELS = [
    ('debit', None, '⊟'),
    ('credit', None, '⊞'),
    ('transfer', 'member net transfer'),
    ('transfer', 'osko'),
    ('info', 'invalid pin'),
    ('info', 'rate change'),
    ('fee', 'transaction fee'),
    ('fee', 'visa access card fee'),
    ('fee', 'overdraft administration fee'),
    ('fee', 'overdraft interest'),
    ('fee', 'bank cheque fee'),
    ('atm', 'atm#'),
    ('atm', 'atm owner fee'),
    ('fee', 'atm owner fee'),
    ('visa', 'visa', '💳'),
    ('direct debit', 'direct debit'),
    ('pos', 'pos #'),
    ('pos', 'pos including cash out'),
    ('cash-out', 'pos including cash out'),
    ('bpay', 'bpay'),
]


def insert_default_labels(db):
    for label_name, regex, *label_display in DEFAULT_LABELS:
        if label_display:
            label_display = label_display[0]
        else:
            label_display = None
        database.insert_label(db, label_name, regex, label_display)


def insert_card_label(db, label_name, card_number, label_display=None):
    for regex in card_label_regex(card_number):
        database.insert_label(db, label_name, regex, label_display)


def card_label_regex(card_number):
    yield r'(au|gb|us|lu|nl|hk)(|frgn amt-[\d\.]+)#{}'.format(card_number)
    yield r'visa access card fee <va\s*-{}\s*>'.format(card_number[:-1])


DEFAULT_CLEANING_RULES = [
    (r'\(?ref[#\.]?\d+\)?', '<ref>'),
    (r'(- )?rec(\.|eipt) no\.?:? \d+\.?( crn#\d+)?\s*', '<ref>'),
    (r'ext tfr - net# \d+ to \d+', 'ext tfr <ref>'),
    (r'atm#\d+', 'atm'),
    (r'atm owner fee \d+ ', 'atm fee-'),
    (r'pos\s*#\d+', 'pos'),
    (r'pos including cash out\s*#\d+', 'pos+cash'),
    (r'internet bpay to ', 'bpay-'),
    (r'bpay payment to ([\w\s]+) (\d+)', r'bpay-\1 - biller \2'),
    (r'- biller code (\d+)', r'- biller \1'),
    (r'(au|gb|us|lu|nl|hk)(|frgn amt-[\d\.]+)#\d+(<ref>|\(ref\.\d+\))', ''),
]


def insert_default_cleaning_rules(db):
    for regex, subst in DEFAULT_CLEANING_RULES:
        database.insert_cleaning_rule(db, regex, subst)


def reprocess_descriptions(db):
    database.delete_cleaned_descriptions(db)


def cleanup_descriptions(db):
    rules = [{'rule_id': rule['rule_id'],
              'regex': re.compile(rule['regex']),
              'subst': rule['subst']}
             for rule in database.load_cleaning_rules(db)]

    for transaction in database.load_unprocessed_transactions(db):
        cleaned_desc, rules_used \
            = cleanup_description(transaction['description'], rules)

        database.insert_cleaned_description(
            db, transaction['transaction_id'], cleaned_desc)

        for rule_id in rules_used:
            database.insert_cleaning_rules_used(
                db, transaction['transaction_id'], rule_id)


def cleanup_description(description, rules):
    cleaned_desc = re.sub('\s+', ' ', description).lower().strip()
    rules_used = []

    for rule in rules:
        def handle(match):
            rules_used.append(rule['rule_id'])
            return match.expand(rule['subst'])

        cleaned_desc = rule['regex'].sub(handle, cleaned_desc)

    return cleaned_desc, rules_used


def reprocess_labels(db):
    database.delete_labelled_transactions(db)


def label_transactions(db):
    rules = [{'rule_id': rule['rule_id'],
              'regex': re.compile(rule['regex'], re.IGNORECASE),
              'label_id': rule['label_id']}
             for rule in database.load_label_rules(db)]

    credit_label_id = database.insert_label(db, 'credit', None)
    debit_label_id = database.insert_label(db, 'debit', None)

    labels = partial(label_transaction, rules, credit_label_id, debit_label_id)

    for transaction in database.load_unlabelled_transactions(db):
        for label_id, rule_id in labels(transaction['description'],
                                        transaction['amount']):
            database.label_transaction(
                db, transaction['transaction_id'], label_id, rule_id)


def label_transaction(rules, credit_label_id, debit_label_id,
                      description, amount):
    if amount > 0:
        yield credit_label_id, None

    if amount < 0:
        yield debit_label_id, None

    for rule in rules:
        if rule['regex'].search(description):
            yield rule['label_id'], rule['rule_id']
