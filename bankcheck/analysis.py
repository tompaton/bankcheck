from operator import itemgetter
from . import database


def transactions_view(db, N=100):
    _transactions_view(database.transactions_view(db)[:N])


def _transactions_view(rows):
    print('{:4} {:10} {:12} {:60} {}'
          .format('Acc#', 'Date', 'Amount', 'Description', 'Labels'))
    for row in rows:
        print('{:4} {:>10} {:>12} {:60} {}'
              .format(row['account_id'],
                      row['iso_entered_date'],
                      row['amount_str'],
                      row['cleaned_desc'],
                      row['label_names']))
    print()


def one_off_credits(db, N=100):
    print('One-off Credits')
    credit_label_id = database.insert_label(db, 'credit', None)
    _transactions_view(database.one_off_transactions(db, credit_label_id)[:N])


def recurring_credits(db, N=100):
    print('Recurring Credits')
    credit_label_id = database.insert_label(db, 'credit', None)
    _transactions_view(database.recurring_transactions(db, credit_label_id)[:N])


def one_off_debits(db, N=100):
    print('One-off Debits')
    debit_label_id = database.insert_label(db, 'debit', None)
    _transactions_view(database.one_off_transactions(db, debit_label_id)[:N])


def recurring_debits(db, N=100):
    print('Recurring Debits')
    debit_label_id = database.insert_label(db, 'debit', None)
    _transactions_view(database.recurring_transactions(db, debit_label_id)[:N])


def unlabelled_transactions(db, N=100):
    print('Unlabelled Transactions')
    rows = database.load_unlabelled_transactions(db, filter_debit_credit=True)
    _transactions_view(rows[:N])


def cleaning_rules_used(db):
    print('Count   Id    Cleaning Rule')
    rows = sorted(database.cleaning_rules_used(db),
                  key=itemgetter('count'), reverse=True)
    for row in rows:
        print('{count:5d}: {rule_id:3d} "{regex}" --> "{subst}"'
              .format(**row))
    print()


def label_rules_used(db):
    print('Count   Id Label Rule')
    rows = sorted(database.label_rules_used(db),
                  key=itemgetter('count'), reverse=True)
    for row in rows:
        print('{count:5d}: {rule_id:3d} "{regex}" --> "{label_name}"'
              .format(**row))
    print()


def labels_used(db):
    print('Count   Id Label')
    rows = sorted(database.labels_used(db),
                  key=itemgetter('count'), reverse=True)
    for row in rows:
        print('{count:5d}: {label_id:3d} "{label_name}"'
              .format(**row))
    print()


def last_entered_date(db):
    print('{:4} {:30} {:10} {:10} {:>10}'
          .format('Acc#', 'Account Name', 'Number',
                  'Latest Date', 'Latest Balance'))

    for row in database.get_accounts(db):
        print('{account_id:4d} {account_name:30} {account_number:10} '
              '{last_iso_entered_date:10} {last_balance_str:>10}'
              .format(**row))

    print()
