from datetime import date, datetime
from unittest.mock import Mock

from . import database


def test_latest_transactions_date_imported():
    db = Mock()
    db.execute.return_value = ['query-result']

    assert database.latest_transactions(
        db, 123, date_imported=datetime(2018, 6, 1, 9, 8, 7)) \
        == ['query-result']

    db.execute.assert_called_with(
        'SELECT T.*, A.amounts '
        'FROM transactions_view T '
        'INNER JOIN all_amounts A '
        'ON T.cleaned_desc = A.cleaned_desc '
        'WHERE T.account_id = ? '
        'AND T.date_imported >= ?',
        (123, date(2018, 6, 1)))


def test_latest_transactions_days():
    db = Mock()
    db.execute.return_value = ['query-result']

    assert database.latest_transactions(
        db, 123, days=2, _today=date(2018, 6, 10)) \
        == ['query-result']

    db.execute.assert_called_with(
        'SELECT T.*, A.amounts '
        'FROM transactions_view T '
        'INNER JOIN all_amounts A '
        'ON T.cleaned_desc = A.cleaned_desc '
        'WHERE T.account_id = ? '
        'AND T.iso_entered_date >= ?',
        (123, '2018-06-08'))
