import requests
import traceback


class BankAustralia():

    def __init__(self, login_url, csv_url, uid, pwd, session_callback=None):
        self.login_url = login_url
        self.csv_url = csv_url
        self.uid = uid
        self.pwd = pwd
        self.session_callback = session_callback

    def get_csv(self, acc, date_from, date_to):
        session = requests.session()
        if callable(self.session_callback):
            self.session_callback(session)
        try:
            session.post(self.login_url,
                         data={'MemberNumber': self.uid,
                               'Password': self.pwd})

            data = {'rdoTransctionType': '46',
                    'TransactionTypeId': '40',
                    'TransactionPeriod': 'Selected date range',
                    'BeginDate': '{0:%Y-%m-%d}T00:00:00.000'.format(date_from),
                    'EndDate': '{0:%Y-%m-%d}T00:00:00.000'.format(date_to),
                    'TransactionOrder': '1',
                    'DateFormat': 'dd/MM/yyyy',
                    'TransactionTypeDesc': 'ALL',
                    'AccountNumber': acc}

            return session.post(self.csv_url, data=data).content

        except requests.exceptions.RequestException:
            traceback.print_exc()
            return ''
