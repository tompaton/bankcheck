from datetime import date
from unittest.mock import Mock, patch, sentinel, call

from . import fetch


@patch.object(fetch, 'requests')
def test_BankAustralia_get_csv(mock_requests):
    response = Mock(content=sentinel.csv_result)
    mock_requests.session().post.side_effect = [None, response]

    bank = fetch.BankAustralia('bank.url/login', 'bank.url/csv',
                               '1234', 'secret')

    assert bank.get_csv('4321',
                        date(2017, 8, 20),
                        date(2017, 8, 25)) == sentinel.csv_result

    assert mock_requests.session.mock_calls == [
        call(),
        call(),
        call().post('bank.url/login',
                    data={'MemberNumber': '1234', 'Password': 'secret'}),
        call().post('bank.url/csv',
                    data={'rdoTransctionType': '46',
                          'TransactionTypeId': '40',
                          'TransactionPeriod': 'Selected date range',
                          'BeginDate': '2017-08-20T00:00:00.000',
                          'EndDate': '2017-08-25T00:00:00.000',
                          'TransactionOrder': '1',
                          'DateFormat': 'dd/MM/yyyy',
                          'TransactionTypeDesc': 'ALL',
                          'AccountNumber': '4321'}),
    ]
