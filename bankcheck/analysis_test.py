from unittest.mock import patch, call

from . import analysis


@patch('builtins.print')
def test_transactions_view(mocked_print):
    analysis._transactions_view([{'account_id': 1,
                                  'iso_entered_date': '2010-02-01',
                                  'amount_str': '-$123.45',
                                  'cleaned_desc': 'lorum ipsum dolor',
                                  'label_names': 'debit,visa'}])

    assert mocked_print.mock_calls == [
        call('Acc# Date       Amount       Description                  '
             '                                Labels'),
        call('   1 2010-02-01     -$123.45 lorum ipsum dolor            '
             '                                debit,visa'),
        call(),
    ]
