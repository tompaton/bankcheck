import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="bankcheck",
    version="1.2.0",
    author="Tom Paton",
    author_email="tom.paton@gmail.com",
    description="Classify transactions from bank statements",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://bitbucket.com/tompaton/bankcheck",
    packages=setuptools.find_packages(),
    classifiers=(
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ),
    install_requires=[
        'pytest',
        'requests',
    ],
)
